import unittest
import random
import os

from flask import current_app

import app.main.util.pypy_sha256 as pypy_sha256
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import ec
from app.main.util.crypto import cu
from app.main.service.rpc_service import generator, prover, verifier
from app.test.base import BaseTestCase

class TestHashCompatibility(BaseTestCase):


    def test_provable_sha_same_input_as_libsnark(self):
        """
        libsnark test is based on creating two vectors of 32bits words with seed(0):
        left: {0x426bc2d8, 0x4dc86782, 0x81e8957a, 0x409ec148, 0xe6cffbe8, 0xafe6ba4f, 0x9c6f1978, 0xdd7af7e9}
        right: {0x038cce42, 0xabd366b8, 0x3ede7e00, 0x9130de53, 0x72cdf73d, 0xee825114, 0x8cb48d1b, 0x9af68ad0}
        output: {0xeffd0b7f, 0x1ccba116, 0x2ee816f7, 0x31c62b48, 0x59305141, 0x990e5c0a, 0xce40d33d, 0x0b1167d1}
        """
        #this is the two bytes vector used to test in libsnark:
        left = [216, 194, 107, 66, 130, 103, 200, 77, 122, 149, 232, 129, 72, 193, 158, 64, 232, 251, 207, 230, 79, 186, 230, 175, 120, 25, 111, 156, 233, 247, 122, 221]
        right = [66, 206, 140, 3, 184, 102, 211, 171, 0, 126, 222, 62, 83, 222, 48, 145, 61, 247, 205, 114, 20, 81, 130, 238, 27, 141, 180, 140, 208, 138, 246, 154]
        #transform into bytes message
        int_message = left+right
        bytes_array = [(bytes([b])) for b in int_message] #the ints are all <256, bytes[] is therefore safe
        bytes_message = bytes()
        for lb in bytes_array:
            bytes_message += lb
        
        test_res = cu.provable_sha256_transform(bytes_message)

        expected_output = [0xeffd0b7f, 0x1ccba116, 0x2ee816f7, 0x31c62b48, 0x59305141, 0x990e5c0a, 0xce40d33d, 0x0b1167d1]
        output_words = test_res['output'].split(",")
        for i in range(0, len(output_words)):
            self.assertTrue(hex(int(output_words[i],16)) == hex(expected_output[i]))

    def test_single_primary_input_from_libsnark(self):
        """
        Check that the value that libsnark outputs as primari_input, which is a file with 256 (number of inputs) as first line and then a 0 or 1
        for each of the 256 bits of the expected hash.
        """
        
        expected_output = cu.words_to_bytes([0xeffd0b7f, 0x1ccba116, 0x2ee816f7, 0x31c62b48, 0x59305141, 0x990e5c0a, 0xce40d33d, 0x0b1167d1])
        primary_in = ""
        fileDir = os.path.dirname(os.path.realpath('__file__'))

        #this works only when the test command is launched from the eonbasics folder
        with open('app/test/zkp_files/sha256_primary_input_data', 'r') as file:
            primary_in = file.read().replace('\n', '')

        #check integrity
        bit_count = primary_in [0:3]
        self.assertTrue(bit_count=="256")
        primary_in=primary_in.replace(bit_count,'')

        o_bytes_array = [(bytes([b])) for b in expected_output] #these are 32 bit words
        o_bytes_message = bytes()
        for lb in o_bytes_array:
            o_bytes_message += lb   
        expected_output_int = int.from_bytes(o_bytes_message, byteorder='big') #the words are passed in big endian to the cpp lib, so we need to take it into account
        binary_string_output = ("{0:b}".format(expected_output_int)) #prepare the string of bits
        binary_string_input_reversed = ((primary_in)[::-1]) #prepare the string of bits but reverse them to account for big endian

    def test_eonbridge(self):
        """
        Call the execs for generator and prover, then if the outputfiles are there, finally call the verifier
        """
        expected_output = cu.words_to_bytes([0xeffd0b7f, 0x1ccba110, 0x2ee816f7, 0x31c62b40, 0x59305141, 0x990e5c0a, 0xce40d330, 0x0b1167d0])
        id = "python_test";

        gen_output = generator(expected_output, id)

        prover_output = prover(expected_output, id)

        expected_files = [id+"_vk", id+"_pvk", id+"_pk", id+"_proof"]
        for expected_file in expected_files:
            file_path = current_app.config['EONBRIDGE_FILES_PATH']+"/"+expected_file
            self.assertTrue(os.path.isfile(file_path))

        verifier_output = verifier(gen_output["provable_hash"], id)
        self.assertTrue(verifier_output==1)

        #try the verifier for some other output:
        provable_pack = cu.provable_sha256_transform(b'some message here');
        verifier_output = verifier(provable_pack["output"], id)
        self.assertTrue(verifier_output==-1)




if __name__ == '__main__':
    unittest.main()
