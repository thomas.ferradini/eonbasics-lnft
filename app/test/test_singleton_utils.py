import unittest
from multiprocessing import Pool, TimeoutError, get_context

from app.main.model.txo import Txo
from app.main.util.crypto import cu
from app.test.base import BaseTestCase


def check_txo_with_cu(res):
    """ helper function used to test concurrency with pool.map """
    return cu.check_txout(res[0], res[1])

class TestConcurrentOperationsWithCelery(BaseTestCase):

    def test_celery_operations(self):
        """ open concurrent requests to the node and see how celery reacts, we should not receive node errors """
        utxos = cu.get_utxos()
        self.assertTrue(len(utxos) > 1)
        objects = []
        for txo in utxos:
            objects.append((txo['txid'], txo['n']))
        longlist = objects * 2 if len(objects)<10 else objects #prepare a hefty list of objects to check
        clogged_node_errors = [
            '-342: non-JSON HTTP response with \'400 Bad Request\' '
            'from server',
            'Remote end closed connection without response',
            'timed out',
            '-32700: Parse error'
        ]
        with get_context("spawn").Pool(processes=4) as pool:
            # NOTE:
            # without get_context("spawn") creating a pool in the test class generates a deadlock.
            # it looks like threads are not passed to the child, so the original test never ends
            results = pool.map(check_txo_with_cu, longlist)
            for result in results:
                self.assertTrue(result == 'unspent' and str(result) not in clogged_node_errors)

if __name__ == '__main__':
    unittest.main()
