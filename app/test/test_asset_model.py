import unittest
import requests
import random

from app.main.model.asset import Asset
from app.main.service.asset_service import save_new_asset, get_an_asset, get_all_assets, mint_asset
from app.main.util.crypto import cu
from app.test.base import BaseTestCase
from app.main.util.eonerror import EonError

class TestAssetModel(BaseTestCase):
    DEBUG = False

    def test_asset_creation_bogus_data(self):
        """ try  to insert a tx with bogus data """
        payload = { #without name
            'description': 'Here is the description',
            'external_url': 'https://www.google.com/search?q=external+url+example&rlz=1C1RXQR_itIT934IT934&oq=external+url+example&aqs=chrome..69i57j0i22i30l3j0i10i22i30j0i22i30j0i390i395.5282j1j7&sourceid=chrome&ie=UTF-8',
            'image': 'https://www.google.com/search?q=external+url+example&rlz=1C1RXQR_itIT934IT934&sxsrf=ALeKk00GXlkGX8z-x9CPs7VN6RFPevaUzg:1629190913964&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjZqMqN2bfyAhXtz4UKHTbKDqgQ_AUoAXoECAEQAw&biw=1280&bih=577#imgrc=9jm3MnMqXVJucM',
            'attributes': '45'
        }
        try:
            error_response = save_new_asset(payload)
        except EonError as e:
            self.assertTrue(e.message == "Payload is missing mandatory data")

        payload = { #without external_url
            'description': 'Here is the description',
            'image': 'https://www.google.com/search?q=external+url+example&rlz=1C1RXQR_itIT934IT934&sxsrf=ALeKk00GXlkGX8z-x9CPs7VN6RFPevaUzg:1629190913964&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjZqMqN2bfyAhXtz4UKHTbKDqgQ_AUoAXoECAEQAw&biw=1280&bih=577#imgrc=9jm3MnMqXVJucM',
            'name': 'Testing_name',
            'attributes': '45'
        }
        try:
            error_response = save_new_asset(payload)
        except EonError as e:
            self.assertTrue(e.message == "Payload is missing mandatory data")

        payload = { #without errors
            'description': 'Here is the description',
            'external_url': 'https://www.google.com/search?q=external+url+example&rlz=1C1RXQR_itIT934IT934&oq=external+url+example&aqs=chrome..69i57j0i22i30l3j0i10i22i30j0i22i30j0i390i395.5282j1j7&sourceid=chrome&ie=UTF-8',
            'image': 'https://www.google.com/search?q=external+url+example&rlz=1C1RXQR_itIT934IT934&sxsrf=ALeKk00GXlkGX8z-x9CPs7VN6RFPevaUzg:1629190913964&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjZqMqN2bfyAhXtz4UKHTbKDqgQ_AUoAXoECAEQAw&biw=1280&bih=577#imgrc=9jm3MnMqXVJucM',
            'name': 'Testing_name',
            'attributes': '45'
        }
        try:
            error_response = save_new_asset(payload)
            ddit_explode = False
        except EonError as e:
            ddit_explode = True
        self.assertFalse(ddit_explode)

    def test_asset_creation(self):
        payload = { #without errors
            'description': 'Here is the description',
            'external_url': 'https://www.google.com/search?q=external+url+example&rlz=1C1RXQR_itIT934IT934&oq=external+url+example&aqs=chrome..69i57j0i22i30l3j0i10i22i30j0i22i30j0i390i395.5282j1j7&sourceid=chrome&ie=UTF-8',
            'image': 'https://www.google.com/search?q=external+url+example&rlz=1C1RXQR_itIT934IT934&sxsrf=ALeKk00GXlkGX8z-x9CPs7VN6RFPevaUzg:1629190913964&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjZqMqN2bfyAhXtz4UKHTbKDqgQ_AUoAXoECAEQAw&biw=1280&bih=577#imgrc=9jm3MnMqXVJucM',
            'name': 'Testing_name',
            'attributes': '45'
        }
        response = save_new_asset(payload)
        params=response[0]
        pid=params['public_id']
        new_asset=get_an_asset(pid, True)
        self.assertTrue(payload['name']==new_asset.name)
        self.assertTrue(payload['external_url']==new_asset.external_url)

    def test_get_all_assets(self):
        payload1 = {
            'description': 'Here is the description',
            'external_url': 'https://www.google.com/search?q=external+url+example&rlz=1C1RXQR_itIT934IT934&oq=external+url+example&aqs=chrome..69i57j0i22i30l3j0i10i22i30j0i22i30j0i390i395.5282j1j7&sourceid=chrome&ie=UTF-8',
            'image': 'https://www.google.com/search?q=external+url+example&rlz=1C1RXQR_itIT934IT934&sxsrf=ALeKk00GXlkGX8z-x9CPs7VN6RFPevaUzg:1629190913964&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjZqMqN2bfyAhXtz4UKHTbKDqgQ_AUoAXoECAEQAw&biw=1280&bih=577#imgrc=9jm3MnMqXVJucM',
            'name': 'Testing_name',
            'attributes': '45'
        }
        payload2 = {
            'description': 'Here is the description',
            'external_url': 'https://www.google.com/search?q=external+url+example&rlz=1C1RXQR_itIT934IT934&oq=external+url+example&aqs=chrome..69i57j0i22i30l3j0i10i22i30j0i22i30j0i390i395.5282j1j7&sourceid=chrome&ie=UTF-8',
            'image': 'https://www.google.com/search?q=external+url+example&rlz=1C1RXQR_itIT934IT934&sxsrf=ALeKk00GXlkGX8z-x9CPs7VN6RFPevaUzg:1629190913964&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjZqMqN2bfyAhXtz4UKHTbKDqgQ_AUoAXoECAEQAw&biw=1280&bih=577#imgrc=9jm3MnMqXVJucM',
            'name': 'Test_name',
            'attributes': '451'
        }
        payl1 = save_new_asset(payload1)
        payl2 = save_new_asset(payload2)
        self.assertTrue(len(get_all_assets(True))==2)

    """def test_issuance(self): #TODO
        payload1 = {
            'description': 'Here is the description',
            'external_url': 'https://www.google.com/search?q=external+url+example&rlz=1C1RXQR_itIT934IT934&oq=external+url+example&aqs=chrome..69i57j0i22i30l3j0i10i22i30j0i22i30j0i390i395.5282j1j7&sourceid=chrome&ie=UTF-8',
            'image': 'https://www.google.com/search?q=external+url+example&rlz=1C1RXQR_itIT934IT934&sxsrf=ALeKk00GXlkGX8z-x9CPs7VN6RFPevaUzg:1629190913964&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjZqMqN2bfyAhXtz4UKHTbKDqgQ_AUoAXoECAEQAw&biw=1280&bih=577#imgrc=9jm3MnMqXVJucM',
            'name': 'Testing_name',
            'attributes': '45'
        }
        payl1 = save_new_asset(payload1)
        params=payl1[0]
        asset_id=params['public_id']
        mint_asset(asset_id, False)"""

if __name__ == '__main__':
    unittest.main()
