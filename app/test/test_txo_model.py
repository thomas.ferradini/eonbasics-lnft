import unittest
import requests
import random

from app.main.model.txo import Txo
from app.main.service.txo_service import get_all_txos, save_new_txo, get_a_txo, spend_a_txo
from app.main.util.crypto import cu
from app.test.base import BaseTestCase
from app.main.util.eonerror import EonError

class TestTxoModel(BaseTestCase):
    DEBUG = False

    def test_txo_creation_bogus_data(self):
        """ try  to insert a tx with bogus data """
        payload = {
            'txid': 'lel',
            'voutn': 45,
            'value': 1000
        }
        try:
            error_response = save_new_txo(payload)
        except EonError as e:
            self.assertTrue(e.message == 'Txo does not exists.')
        #random data
        payload = {
            'txid':
            '4801c2fcf44a51ed567a79b215dca18da1e6d9e81e57b8f2a10604b7c4a39000',
            'voutn': 45,
            'value': 1000
        }
        try:
            error_response = save_new_txo(payload)
        except EonError as e:
            self.assertTrue(e.message == 'Txo does not exists.')

    def test_txo_creation_and_spend(self):
        """ create a txo from an unspent txo, then spend it and check the status """
        
        utxos = cu.get_utxos()
        self.assertTrue(isinstance(utxos, list))
        self.assertTrue(len(utxos) >= 1)
        payload = {
            'txid': utxos[0]['txid'],
            'voutn': utxos[0]['n'],
            'value': utxos[0]['amount']
        }
        _ = save_new_txo(payload)
        self.assertTrue(_[1] == 201)
        # check the inserted txo, refresh it from the node:
        inserted_txo = get_a_txo(_[0]['public_id'], 'false')
        self.assertTrue(inserted_txo.status == 'unspent')
        self.assertTrue(inserted_txo.last_checked_blockhash == cu.get_best_blockhash())
        txos = get_all_txos({})
        self.assertTrue(txos['total_record_count'] == 1)
        self.assertTrue(txos['records'][0].txid == inserted_txo.txid)
        txo_local_id = inserted_txo.id
        self.assertTrue(isinstance(txo_local_id, int))
        self.assertTrue(txo_local_id == 1)
        # spent the utxo and test the status change:
        cu.spend_txo(inserted_txo.txid, inserted_txo.voutn, '7e57')
        self.assertTrue(inserted_txo.status == 'spent')
        # since we spent, generate a new block so to empty the mempool
        self.requestNewTestBlock()

    def test_txo_creation_and_spend_msg_too_long(self):
        """ create a txo from an unspent txo, then spend it and check the status """
        
        utxos = cu.get_utxos()
        self.assertTrue(isinstance(utxos, list))
        self.assertTrue(len(utxos) >= 1)
        payload = {
            'txid': utxos[0]['txid'],
            'voutn': utxos[0]['n'],
            'value': utxos[0]['amount']
        }
        _ = save_new_txo(payload)
        self.assertTrue(_[1] == 201)
        # check the inserted txo, refresh it from the node:
        inserted_txo = get_a_txo(_[0]['public_id'], 'false')
        self.assertTrue(inserted_txo.status == 'unspent')
        self.assertTrue(inserted_txo.last_checked_blockhash == cu.get_best_blockhash())
        txos = get_all_txos({})
        self.assertTrue(txos['total_record_count'] == 1)
        self.assertTrue(txos['records'][0].txid == inserted_txo.txid)
        txo_local_id = inserted_txo.id
        self.assertTrue(isinstance(txo_local_id, int))
        self.assertTrue(txo_local_id == 1)
        # spent the utxo and test the status change:
        long_msg = [random.randint(0, 255) for i in range(100)]
        bytes_array = [(bytes([b])) for b in long_msg]
        bytes_message = bytes()
        for lb in bytes_array:
            bytes_message += lb
        data={"msg_hex":bytes_message.hex()}
        got_error=False
        try:
            spend_a_txo(inserted_txo.public_id, data)
        except EonError as ee:
            got_error=True
            self.assertTrue(ee.code == 400)
        self.assertTrue(got_error)
        self.assertTrue(inserted_txo.status == 'unspent')
        # since we spent, generate a new block so to empty the mempool
        self.requestNewTestBlock()

    def test_same_txo_creation(self):
        """ insert two times the same Txo and check it doesn't duplicate """
        
        utxos = cu.get_utxos()
        self.assertTrue(isinstance(utxos, list))
        self.assertTrue(len(utxos) >= 1)
        payload = {
            'txid': utxos[0]['txid'],
            'voutn': utxos[0]['n'],
            'value': utxos[0]['amount']
        }
        _ = save_new_txo(payload)  # inserted_response
        try:
            error_response = save_new_txo(payload)
        except EonError as e:
            self.assertTrue(e.message == 'Txo already exists.')
            self.assertTrue(e.code == 409)
        
        txos = get_all_txos({}) # empty args, will use the default cached=true, page=1, page_size=20
        self.assertTrue(txos['total_record_count'] == 1)

        



if __name__ == '__main__':
    unittest.main()
