import unittest

from app.main.util.crypto import cu
from app.test.base import BaseTestCase


class TestCryptoUtilities(BaseTestCase):
    DEBUG = False

    def test_utxos(self):
        """ Get all the (non-locked) utxos from the node and check their status"""
        
        utxos = cu.get_utxos()
        for utxo in utxos:
            result = cu.check_txout(utxo['txid'], utxo['n'])
            self.assertTrue(result == 'unspent')

    def test_spending(self):
        """ Get a UTXO, spend it, check it appears in the new tx, request a new test-network block and check the confirmation """
        
        utxos = cu.get_utxos()
        self.assertTrue(isinstance(utxos, list))
        self.assertTrue(len(utxos) >= 1)
        sent_with_tx = cu.spend_txo(utxos[0]['txid'], utxos[0]['n'], '7e57')
        result = cu.check_txout(utxos[0]['txid'], utxos[0]['n'])
        self.assertTrue(result == 'spent')
        raw_tx = cu.get_raw_tx(utxos[0]['txid'])
        pre_block_confirmations = 0
        if('confirmations' in raw_tx):
            pre_block_confirmations = raw_tx['confirmations']
        # generate a new block! Use BaseTestCase method
        # PUT to explorer url /block with header: "Api-Key : "
        self.requestNewTestBlock()
        # check that txid has now +1 confirmation
        if(raw_tx != 'RemoteDisconnected'):
            raw_tx = cu.get_raw_tx(utxos[0]['txid'])
            self.assertTrue(raw_tx['confirmations'] > pre_block_confirmations)
        #check the initial seal is indeed included in the new tx:
        check_seal_result = (cu.was_seal_spent_in_tx(utxos[0]['txid'], utxos[0]['n'],sent_with_tx))
        self.assertTrue(check_seal_result["result"])
        self.assertTrue(check_seal_result["confirmations"]==1)


    def test_digest(self):
        """ Test the digest and hexgides """
        
        bytes_digest = cu.digest("test")
        self.assertTrue(len(bytes_digest) > 0)
        hex_digest = cu.hexdigest("test")
        self.assertEqual(bytes_digest.hex(), hex_digest)

    def test_locking(self):
        """ Get a UTXO, lock it and unlock it """
        #IMPORTANT NOTE:
        #this test locks txos, if anything happen and the test doens't end, the txo remains locked on the node and may impact other tests
        #be sure to make this test work properly, when it fails, contact the team and announce that you need to reset the test node
        #locked/unlocked states are store in memory, so restarting the node will clear all the locks
        
        utxos = cu.get_utxos()
        self.assertTrue(isinstance(utxos, list))
        self.assertTrue(len(utxos) >= 1)
        #lock a txid and check that you have one more locked txo:
        locked_unspent = cu.get_locked_utxos()
        cu.lock_utxo(utxos[0]['txid'], utxos[0]['n'])
        self.assertTrue(len(locked_unspent) < len(cu.get_locked_utxos()))
        #unlock the txo
        cu.unlock_utxo(utxos[0]['txid'], utxos[0]['n'])
        #check that there are no more new locked utxos
        locked_unspent_post_test = cu.get_locked_utxos()
        self.assertTrue(len(locked_unspent)==len(locked_unspent_post_test))

    def test_locking_a_utxo_not_yours(self):
        """ try to lock an unkown txo """
        #TODO: is this the same error when trying to lock a txo which is not yours?
        
        utxos = cu.get_utxos()
        self.assertTrue(isinstance(utxos, list))
        self.assertTrue(len(utxos) >= 1)
        #lock a txid and check that you have one more locked txo:
        #manipulate txid:
        txid = utxos[0]['txid']
        k = utxos[0]['txid'].rfind("a")
        txid = utxos[0]['txid'][:k] + "9" + utxos[0]['txid'][k+1:]
        try:
            cu.lock_utxo(txid, 0)
        except Exception as e:
            self.assertTrue("Invalid parameter, unknown transaction" in str(e))

    def test_list_issuances(self):
        """ list the issuances of an asset """
        issuances = cu.list_issuances()
        #there should be at least one asset called "bitcoin"
        self.assertTrue(len(issuances)>=1)

    def test_new_address(self):
        """ create a new address """
        new_addrs = cu.get_new_address("test")
        self.assertTrue(len(new_addrs)>0)

    def test_nft_issuance(self):
        """ test the issuance of an nft """
        issuances = cu.list_issuances()
        new_addrs = cu.get_new_address("test")
        issuance = cu.issue_lnft_asset(False, new_addrs, 'test', 'XTST', 'localhost')
        issuances2 = cu.list_issuances()
        self.assertTrue(len(issuances2)>len(issuances))
        found = False
        for iss in issuances2:
            found = (iss['asset'] == issuance['asset'])
            if found:
                break
        self.assertTrue(found);


if __name__ == '__main__':
    unittest.main()
