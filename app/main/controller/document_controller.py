from flask import request
from flask_restplus import Resource

from app.main.service.document_service import get_a_document, get_all_documents, save_new_document, update_document, get_related_documents, check_seals_lock_status
from app.main.util.dto import DocumentDto
from app.main.util.eonerror import EonError
from app.main.util.decorator import admin_token_required, token_required


api = DocumentDto.api
_document = DocumentDto.document
_new_document = DocumentDto.new_document
_document_to_update = DocumentDto.document_to_update
_documents_page = DocumentDto.paged_document_list

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")

@api.route('/')
class DocumentList(Resource):
    @api.doc('List of documents registered on this node')
    @api.doc(params={'cached': {'description':'true/false, whether return connected txo status from the local db or query the node, defaults to true', 'in': 'query', 'type': 'bool'},
    'page': {'description': 'desired page number', 'in': 'query', 'type': 'int'},
    'page_size': {'description': 'desired number of records per page', 'in': 'query', 'type': 'int'}
    })
    @api.marshal_with(_documents_page)
    @api.response(400, 'Malformed URL.')
    @api.response(500, 'Internal Server Error.')
    def get(self):
        """Returns docuemnts in a paged list"""
        try:
            return get_all_documents(request.args)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)


    @api.doc('Register a new document')
    @api.expect(parser, _new_document, validate=True)
    @api.response(201, 'Document successfully created.')
    @api.response(409, 'Another document already exists with the given seals.')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def post(self):
        """Register a new document"""
        data = request.json
        try:
            return save_new_document(data=data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)


@api.route('/<public_id>')
@api.doc(params={'cached': {'description':'true/false, whether to return data from the local db or query the node, defaults to true', 'in': 'query', 'type': 'bool'}})
@api.param('public_id', 'The document identifier')
class Document(Resource):
    @api.doc('Get a document')
    @api.marshal_with(_document)
    @api.response(404, 'Document not found.')
    @api.response(500, 'Internal Server Error.')
    def get(self, public_id):
        """Returns a document given its identifier """
        try:
            return get_a_document(public_id, request.args.get("cached"))
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

    
    @api.doc('Update an existing document')
    @api.expect(parser, _document_to_update, validate=True)
    @api.response(200, 'Document successfully updated.')
    @api.response(404, 'Document not found.')
    @api.response(409, 'Conflicts with the request, detailed message included')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def put(self, public_id):
        """Update an existing document"""
        try:
            data = request.json
            return update_document(data=data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>/rpc/send')
@api.param('public_id', 'The document identifier')
class Document(Resource):
    @api.doc('Send an existing document')
    @api.response(200, 'Document successfully sent.')
    @api.response(404, 'Document not found.')
    @api.response(409, 'Conflicts with the request, detailed message included')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def put(self, public_id):
        """Send an existing document"""
        try:
            #TODO: TEST CLASS
            return send_document(public_id)
            return
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/<public_id>/chain')
@api.param('public_id', 'The document identifier')
class Document(Resource):
    @api.doc('Get the chain of document containing the given one')
    @api.marshal_with(_document, as_list=True)
    @api.response(404, 'Document not found.')
    @api.response(500, 'Internal Server Error.')
    def get(self, public_id):
        """Returns the chain of documents containing the desired one"""
        try:
            return get_related_documents(public_id)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

@api.route('/rpc/check-seals')
class DocumentSealsCheck(Resource):
    @api.doc('Check all seals on docs are locked')
    @api.expect(parser, validate=True)
    @api.response(200, 'All documents checked')
    @api.response(401, 'Unauthorised')
    @api.response(500, 'Internal Server Error.')
    @admin_token_required
    def post(self):
        """Register a new document"""
        data = request.json
        try:
            return check_seals_lock_status(data=data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)