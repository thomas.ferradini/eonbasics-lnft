from flask import request
from flask_restplus import Resource

from app.main.service.txo_service import (get_a_txo, get_all_txos, save_new_txo, spend_a_txo)
from app.main.util.dto import TxoDto
from app.main.util.eonerror import EonError
from app.main.util.decorator import admin_token_required, token_required

api = TxoDto.api
_txo = TxoDto.txo
_new_txo = TxoDto.new_txo
_txos_page = TxoDto.paged_txo_list
_spend_txo = TxoDto.spend_txo

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")

@api.route('/')
class TxoList(Resource):
    @api.doc('List of txos registere on this node')
    @api.doc(params={'cached': {'description':'true/false, whether to return data from the local db or query the node, defaults to true', 'in': 'query', 'type': 'bool'},
    'page': {'description': 'desired page number, defaults to 1', 'in': 'query', 'type': 'int'},
    'page_size': {'description': 'desired number of records per page, defaults to 20', 'in': 'query', 'type': 'int'}
    })
    @api.marshal_with(_txos_page)
    @api.response(400, 'Malformed URL.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def get(self):
        """Returns txos in a paged list"""
        try:
            return get_all_txos(request.args)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

    @api.doc('Register a new txo')
    @api.expect(parser, _new_txo, validate=True)
    @api.response(201, 'Txo successfully created.')
    @api.response(400, 'Txo input data is invalid.')
    @api.response(409, 'Txo already exists.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def post(self):
        """Register a new Txo"""
        data = request.json
        try:
            return save_new_txo(data=data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)


@api.route('/<public_id>')
@api.doc(params={'cached': {'description':'true/false, whether to return data from the local db or query the node, defaults to true', 'in': 'query', 'type': 'bool'}})
@api.param('public_id', 'The Txo identifier')
class Txo(Resource):
    @api.doc('get a txo')
    @api.marshal_with(_txo)
    @api.response(404, 'Txo not found.')
    @api.response(500, 'Internal Server Error.')
    def get(self, public_id):
        """Get details for a single txo""" 
        try:
            return get_a_txo(public_id, request.args.get("cached"))
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

"""
Does Eonpass really need this?
"""
"""
@api.route('/<public_id>/rpc/spend')
@api.param('public_id', 'The Txo identifier')
class Txo(Resource):
    @api.doc('spend a txo')
    @api.marshal_with(_spend_txo)
    @api.response(404, 'Txo not found.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def put(self, public_id):
        data = request.json
        try:
            return spend_a_txo(public_id, data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)
"""
