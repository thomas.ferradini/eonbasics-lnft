from flask import request
from flask_restplus import Resource

from app.main.service.asset_service import (get_an_asset, save_new_asset, get_all_assets)
from app.main.util.dto import AssetDto
from app.main.util.eonerror import EonError
from app.main.util.decorator import admin_token_required, token_required

api = AssetDto.api
_asset = AssetDto.asset
_new_asset = AssetDto.new_asset

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")

@api.route('/')
class AssetList(Resource):
    @api.doc('List of assets registered on this node')
    @api.doc(params={'cached': {'description':'true/false, whether to return data from the local db or query the node, defaults to true', 'in': 'query', 'type': 'bool'},
    'page': {'description': 'desired page number, defaults to 1', 'in': 'query', 'type': 'int'},
    'page_size': {'description': 'desired number of records per page, defaults to 20', 'in': 'query', 'type': 'int'}
    })
    @api.marshal_with(_asset, as_list=True)
    #TODO: fare le pagine anche per gli asset
    @api.response(400, 'Malformed URL.')
    @api.response(500, 'Internal Server Error.')
    #@api.expect(parser)
    #@admin_token_required
    def get(self):
        """Returns assets in a paged list"""
        try:
            return get_all_assets(request.args.get("cached"))
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)

    @api.doc('Register a new asset')
    @api.expect(parser, _new_asset, validate=True)
    @api.response(201, 'Asset successfully created.')
    @api.response(400, 'Asset input data is invalid.')
    @api.response(409, 'Asset already exists.')
    @api.response(500, 'Internal Server Error.')
    #@api.expect(parser)
    #@admin_token_required
    def post(self):
        """Register a new Asset"""
        data = request.json
        try:
            return save_new_asset(data=data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)


@api.route('/<public_id>')
@api.doc(params={'cached': {'description':'true/false, whether to return data from the local db or query the node, defaults to true', 'in': 'query', 'type': 'bool'}})
@api.param('public_id', 'The Asset identifier')
class Asset(Resource):
    @api.doc('get an asset')
    @api.marshal_with(_asset)
    @api.response(404, 'Asset not found.')
    @api.response(500, 'Internal Server Error.')
    def get(self, public_id):
        """Get details for a single asset"""
        try:
            return get_an_asset(public_id, request.args.get("cached"))
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)
