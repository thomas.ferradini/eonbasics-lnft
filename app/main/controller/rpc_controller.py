from flask import request
from flask_restplus import Resource


from app.main.service.rpc_service import check_signature, get_local_pub_key, get_new_address

from app.main.util.dto import MethodResultDto
from app.main.util.eonerror import EonError
from app.main.util.decorator import admin_token_required, token_required

api = MethodResultDto.api
_method_result = MethodResultDto.method_result

parser = api.parser()
parser.add_argument('Authorization', location='headers', help="Auth token from login")


@api.route('/check-signature')
class SignatureCalls(Resource):
    @api.doc('Method for checking signatures')
    @api.marshal_with(_method_result)
    @api.response(200, 'Method executed, check result.')
    @api.response(400, 'Bad request, invalid input data format.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def post(self):
        data = request.json
        try:
            return check_signature(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)
    @api.doc('Method for getting the public key')
    @api.marshal_with(_method_result)
    @api.response(200, 'Method executed, check result.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @token_required
    def get(self):
        try:
            return get_local_pub_key()
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)
        except Exception as general_e:
            api.abort(500)

@api.route('new-address')
class SignatureCalls(Resource):
    @api.doc('Method for getting a new address from the node')
    @api.marshal_with(_method_result)
    @api.response(200, 'Method executed, check result.')
    @api.response(400, 'Bad request, invalid input data format.')
    @api.response(500, 'Internal Server Error.')
    @api.expect(parser)
    @admin_token_required
    def post(self):
        data = request.json
        try:
            return get_new_address(data)
        except EonError as e:
            if(e.code and e.message):
                api.abort(e.code, e.message)
            else:
                api.abort(500)