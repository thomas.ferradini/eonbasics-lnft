from bitcoinrpc.authproxy import JSONRPCException
from flask import current_app

from app.main.util.serviceauthproxyfactory import ServiceAuthProxyFactory
from app.main.celery import celery

def _get_rpc_connection():
    """ Get an RPC connection to the blockchain node """
    #NOTE: the builder can use different parameters depending on the desired target blockchain, for now we have only liquid
    factory = ServiceAuthProxyFactory()
    return factory.create(
        'liquid',
        rpc_user=current_app.config['RPC_USER'],
        rpc_password=current_app.config['RPC_PASSWORD'],
        rpc_host=current_app.config['RPC_HOST'],
        rpc_port=current_app.config['RPC_PORT'])()

@celery.task()
def make_rpc_call(method, *args):
    """ exectue the desired method call """
    try:
        rpc_connection = _get_rpc_connection()
        res = getattr(rpc_connection, method)(*args)
        return {'result': res}
    except JSONRPCException as json_exception:
        return {
            'error': "A JSON RPC Exception occured: " + str(json_exception)}
    except Exception as general_exception:
        return {
            'error': "An Exception occured: " + str(general_exception)}
