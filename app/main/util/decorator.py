from functools import wraps
from flask import request

from app.main.service.auth_helper import Auth


def token_required(f):
    """ Checks if the incoming request has a valid authorization header """
    @wraps(f)
    def decorated(*args, **kwargs):
        data, status = Auth.get_logged_in_user(request)
        token = data.get('data')
        if not token:
            return data, status
        return f(*args, **kwargs)
    return decorated


def admin_token_required(f):
    """ Checks if the incoming request has a valid authorization header for admin users"""
    @wraps(f)
    def decorated(*args, **kwargs):
        data, status = Auth.get_logged_in_user(request)
        token = data.get('data')
        if not token:
            return data, status
        admin = token.get('admin')
        if not admin:
            response_object = {
                'status': 'fail',
                'message': 'admin token required'
            }
            return response_object, 401
        return f(*args, **kwargs)
    return decorated


def log_error(logger):
    """ Log the exception and re-rasie it 

        TODO: use this to rationalise logging when output to file
    """
    def decorated(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except Exception as e:
                if logger:
                    logger.exception(e)
                raise
        return wrapped
    return decorated
