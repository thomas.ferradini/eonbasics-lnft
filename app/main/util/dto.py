from flask_restplus import Namespace, fields

"""
Data Transfer Objects

these are used by the controllers to create the swagger documentation
and to cast the dict received by the services into their respective JSONs

"""

class UserDto:
    api = Namespace('user', description='user related operations')
    user = api.model('user', {
        'email': fields.String(required=True,
                               description='user email address'),
        'username': fields.String(required=True, description='user username'),
        'public_id': fields.String(description='user Identifier')
    })
    new_user = api.model('new_user', {
        'email': fields.String(required=True,
                               description='user email address'),
        'username': fields.String(required=True, description='user username'),
        'password': fields.String(required=True, description='user password')
    })

class AuthDto:
    api = Namespace('auth', description='authentication related operations')
    user_auth = api.model('auth_details', {
        'email': fields.String(required=True, description='The email address'),
        'password': fields.String(required=True,
                                  description='The user password'),
    })

class TxoDto:
    api = Namespace('txo', description='txo related operations')
    txo = api.model('txo', {
        'public_id': fields.String(description='document Identifier'),
        'txid': fields.String(required=True,
                              description='The tx id of the output'),
        'voutn': fields.Integer(required=True,
                                description='The index in the output array '
                                            'of the tx'),
        'value': fields.Fixed(decimals=8, require=False,
                              description='Value of the specified output'),
        'last_checked_status': fields.String(required=False,
                                             description='Status of the txo '
                                                         'last time it was '
                                                         'checked - spent or '
                                                         'unspent'),
        'last_checked_time': fields.DateTime(dt_format=u'iso8601',
                                             required=False,
                                             description='Last time this txo '
                                                         'was checked on the '
                                                         'blockchain'),
        'last_checked_blockhash': fields.String(required=False,
                                             description='Most recent blockchash last time the txo was checked'),
    })
    new_txo = api.model('new_txo', {
        'txid': fields.String(required=True,description='The tx id of the output'),
        'voutn': fields.Integer(required=True,description='The index in the outputs array of the tx'),
    })
    paged_txo_list = api.model('paged_txo_list', {
        'page': fields.Integer(description='Page number in the paged list'),
        'page_size': fields.Integer(description='Records per page'),
        'total_record_count': fields.Integer(description='Total records in the list'),
        'records':fields.List(fields.Nested(txo)),
    })
    spend_txo = api.model('spend_txo', {
        'amount': fields.String(required=False,description='The amount to spend out of the txo amount, defaults to all'),
        'msg_hex': fields.String(required=False,description='The hex representation of a string to put in the OP_RETURN'),
        'target_address': fields.String(required=False,description='The addres where to send amount, the rest is sent to the change address'),
    })

class AssetDto: 
    api = Namespace('asset', description='asset related operations')
    asset = api.model('asset', {
        'public_id': fields.String(description='document Identifier'),
        'description': fields.String(required=False,
                              description='Description saved inside the asset'),
        'external_url': fields.String(required=True,
                                description='External url of the asset'),
        'image': fields.String(decimals=8, require=False,
                              description='Image of the asset'),
        'name': fields.String(required=True,
                                             description='Name of the asset'),
        'attributes': fields.String(required=False,
                                            description='Attributes of the asset'),
        'blockchain': fields.String(required=False,
                                            description='Blockchain of the asset'),
        'last_checked_time': fields.DateTime(dt_format=u'iso8601',
                                             required=False,
                                             description='Last time this asset '
                                                         'was checked on the '
                                                         'blockchain'),
        'last_checked_status': fields.String(required=False,
                                             description='Most recent status last time the asset was checked'),
        'last_checked_blockhash': fields.String(required=False,
                                             description='Most recent blockchash last time the asset was checked'),
    })
    new_asset = api.model('new_asset', {
        'description': fields.String(required=False,description='The description of the asset'),
        'external_url': fields.String(required=True,description='The external url of the asset'),
        'image': fields.String(required=False,description='The image of the asset'),
        'name': fields.String(required=True,description='The name of the asset'),
        'attributes': fields.String(required=False,description='The attributes of the asset')
    })

class DocumentDto:
    api = Namespace('document', description='document related operations')
    document = api.model('document', {
        'public_id': fields.String(description='document Identifier'),
        'document_hash': fields.String(
            required=True, description='hash of the document to be notarised'),
        'current_seal': fields.String(
            required=True, description='id of the txo which is current seal'),
        'status':fields.String(
            required=False, description='status of the document', readOnly=True),
        'next_seal': fields.String(
            required=False, description='id of the txo which is next seal'),
        'sender_signed_hash': fields.String(
            required=False, description='document hash signed by the sender '
                                        'with the respective public key'),
        'sender_public_key': fields.String(
            required=False, description='public key used to check if the '
                                        'sender actually signed the message'),
        'sender_and_receiver_signed_hash': fields.String(
            required=False, description='document hash signed by the sender '
                                        'and subsequently by the receiver '
                                        'with the respective public key'),
        'receiver_public_key': fields.String(
            required=False, description='public key used to check if the '
                                        'receiver actually signed the message')
    })
    new_document = api.model('new_document', {
        'document_hash': fields.String(
            required=True, description='hash of the document to be notarised'),
        'current_seal': fields.String(
            required=True, description='id of the txo which is current seal '),
        'next_seal': fields.String(
            required=False, description='id of the txo which is next seal ')
    })
    document_to_update = api.model('document_to_update', {
        'public_id': fields.String(description='document Identifier'),
        'document_hash': fields.String(
            required=True, description='hash of the document to be notarised'),
        'current_seal': fields.String(
            required=True, description='id of the txo which is current seal'),
        'next_seal': fields.String(
            required=False, description='id of the txo which is next seal'),
        'sender_signed_hash': fields.String(
            required=False, description='document hash signed by the sender '
                                        'with the respective public key'),
        'sender_public_key': fields.String(
            required=False, description='public key used to check if the '
                                        'sender actually signed the message'),
        'sender_and_receiver_signed_hash': fields.String(
            required=False, description='document hash signed by the sender '
                                        'and subsequently by the receiver '
                                        'with the respective public key'),
        'receiver_public_key': fields.String(
            required=False, description='public key used to check if the '
                                        'receiver actually signed the message')
    })
    paged_document_list = api.model('paged_document_list', {
        'page': fields.Integer(description='Page number in the paged list'),
        'page_size': fields.Integer(description='Records per page'),
        'total_record_count': fields.Integer(description='Total records in the list'),
        'records':fields.List(fields.Nested(document)),
    })

class MethodResultDto:
    api = Namespace('method_result', description='special methods rpc-like')
    method_result = api.model('method_result', {
        'method': fields.String(required=True, description='The called method'),
        'result': fields.String(required=True, description='The result'),
    })
