from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException

class ServiceAuthProxyFactory:
    def __init__(self):
        self._builders = {}

    def register_builder(self, key, builder):
        """ register new builders other than the default one for liquid/regelements"""
        self._builders[key] = builder

    def create(self, key, **kwargs):
        """ return the appropriate builder """
        builder = self._builders.get(key)
        if not builder:
            # return default
            builder = DefaultAuthServiceProxyBuilder(**kwargs)
        return builder(**kwargs)

class DefaultAuthServiceProxy:
    """ Connection to a regelement/liquid node, uses getblockchaininfo to test the connection """
    def __init__(self, rpc_connection):
        self._instance = rpc_connection

    def __call__(self):
        return self._instance

    def test_connection(self):
        """ try the connection: call the node and return blockchaininfo """
        try:
            return self._instance.getblockchaininfo()
        except JSONRPCException as json_exception:
            print("A JSON RPC Exception occured: " + str(json_exception))
            raise json_exception
        except Exception as general_exception:
            print("An Exception occured: " + str(general_exception))
            raise general_exception

class DefaultAuthServiceProxyBuilder:
    """ Builder for connection to regelements/liquid nodes """
    def __init__(self, **_ignored):
        self._instance = None

    def __call__(self, rpc_user, rpc_password, rpc_host, rpc_port, **_ignored):
        if not self._instance:
            rpc_connection = self.connect(
                rpc_user, rpc_password, rpc_host, rpc_port)
            self._instance = DefaultAuthServiceProxy(rpc_connection)
        return self._instance

    def connect(self, rpc_user, rpc_password, rpc_host, rpc_port):
        try:
            return AuthServiceProxy("http://%s:%s@%s:%s" % (
                rpc_user, rpc_password, rpc_host, rpc_port))
        except JSONRPCException as json_exception:
            print("A JSON RPC Exception occured: " + str(json_exception))
            raise json_exception
        except Exception as general_exception:
            print("An Exception occured: " + str(general_exception))
            raise general_exception
