class Singleton(object):
    _instance = None  # Keep instance reference

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = object.__new__(cls, *args, **kwargs)
        return cls._instance


class EonUtils(Singleton):
    """ Collection of methods used in the Eonpass protocol"""
    def concatenate_hash_and_txos(self, text, current_seal_txid, current_seal_voutn, next_seal_txid, next_seal_voutn):
        """ 
        get the formatted eonpass message: text followed by current and next seal details
        
        concatenation of different parts is done with ";", concatenation of txid and vout index with ","

        Parameters
        ----------
        text: str
            the message, usuallt it's a hex string of a digest

        current_seal_txid: str
            txid of the utxo used as current seal

        current_seal_voutn: int
            voutn of the utxo used as current seal

        next_seal_txid: str
            txid of the utxo used as next seal

        next_seal_voutn: int
            voutn of the utxo used as next seal

        Returns
        -------
        str
            the concatenation of all the inputs

        Raises
        ------
        Exception
            when one of the parameters is missing

        """
        if(not current_seal_txid or not str(current_seal_voutn) or not next_seal_txid or not str(next_seal_voutn) or not text):
            raise Exception('Concatenation of hash and txos is missing arguments')
        message = text+';'+current_seal_txid+',' +str(current_seal_voutn)+';'+next_seal_txid+','+str(next_seal_voutn)
        return message