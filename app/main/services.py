import logging
import os

from flask_bcrypt import Bcrypt
from flask_sqlalchemy import SQLAlchemy

from app.main.config import config_by_name


db = SQLAlchemy()
flask_bcrypt = Bcrypt()

config_name = os.getenv('EONFLASK_ENV') or 'dev'
log_to_file = config_by_name[config_name].LOG_ON_FILE
if(log_to_file):
	logger = logging.basicConfig(level=logging.DEBUG, filename='eonbasics.log', format='%(asctime)s %(levelname)s:%(message)s')


