from flask import Blueprint, Flask
from flask_restplus import Api

from app.main.config import config_by_name
from app.main.controller.auth_controller import api as auth_ns
from app.main.controller.user_controller import api as user_ns
from app.main.controller.txo_controller import api as txo_ns
from app.main.controller.document_controller import api as document_ns
from app.main.controller.asset_controller import api as asset_ns
from app.main.controller.rpc_controller import api as rpc_ns
from app.main.services import db, flask_bcrypt
from app.main.celery import init_celery


def create_app(**kwargs):
    """ create the flask app, then initalise the services: db, bycrypt, celery; finally preapre the api blueprint"""
    config_name=kwargs.get("config_name")
    app = Flask(__name__)
    app.config.from_object(config_by_name[config_name])

    db.init_app(app)
    flask_bcrypt.init_app(app)
    if(kwargs.get("celery")):
      init_celery(kwargs.get("celery"), app)

    blueprint = Blueprint('api', __name__)
    api = Api(blueprint,
              title='EONBASICS',
              version='0.0.9',
              description='Eonpass reference implementation with restplus web service and JWT')
    api.add_namespace(user_ns, path='/user')
    api.add_namespace(auth_ns)
    api.add_namespace(txo_ns, path='/txo')
    api.add_namespace(document_ns, path='/document')
    api.add_namespace(asset_ns, path='/asset')
    api.add_namespace(rpc_ns, path='/rpc')
    app.register_blueprint(blueprint)

    app.app_context().push()

    return app
