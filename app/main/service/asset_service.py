import uuid
import datetime
import math

from app.main.services import db
from app.main.util.crypto import cu
from app.main.model.asset import Asset
from app.main.util.eonerror import EonError
#from app.main.util.decorator import log_error
# TODO: set the logging preference in the config file

def save_new_asset(data):
    """
    Save a new asset in the local db

    If the same NAME?? and DOMAIN?? already exist, return a 409, duplicated content

    Parameters
    ----------
    data: dict
        an object with the following fields, as described in dto new_txo
        'txid', the id of the transaction
        'voutn', the index of the output of that transaction

    Returns
    -------
        [dict, int]
            an array which contains:
            0: the response_object with the following fields: 'status' success/fail,
            'message' and in case of success the 'public_id' of the new asset
            1: the return code for the successfull operation, 201

    Raises
    ------
    EonError:
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        404: when there is something strange in the args (e.g. txid doesn't exist)
        409: when there is already this asset
    """
    try:
        existing_asset = Asset.query.filter_by(name=data['name'], external_url=data['external_url']).first()
        if not existing_asset:

            new_asset = Asset(
                public_id=str(uuid.uuid4()),
                description=data.get('description', None),
                external_url=data['external_url'],
                image=data.get('image', None),
                name=data['name'],
                attributes=data.get('attributes', None)
            )
            _save_changes(new_asset)
            response_object = {
                'status': 'success',
                'message': 'New asset registered.',
                'public_id': new_asset.public_id
            }
            return response_object, 201
        else:
            raise EonError('Asset already exists.', 409)
    except EonError as eerr:
        raise eerr
    except KeyError:
        raise EonError("Payload is missing mandatory data", 400)
    except Exception:
        raise EonError("Internal Server Error", 500)

def get_an_asset(public_id, cached):
    """
    Returns details of an asset, when cached is false, query the node and get a fresh status

    Every time a non-cached request is done, its results are saved in the last_checked_ fields.

    Parameters
    ----------
    public_id: str
        the id on the db of the desired asset

    cached: bool (but coming from controller this is a str "true"/"false")
        true returns data only form the db, false checks also on the node, defaults to true

    Returns
    -------
    object
        the asset object

    Raises
    ------
    EonError:
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        404: when there is no matching public_id in the local db
    """
    asset = None
    if cached and cached=="false":
        return
    else:
        asset = Asset.query.filter_by(public_id=public_id).first()
        if not asset:
            raise EonError('Asset not found.', 404)
    return asset

def get_all_assets(cached):
    """
    Returns details of an asset, when cached is false, query the node and get a fresh status

    Every time a non-cached request is done, its results are saved in the last_checked_ fields.

    Parameters
    ----------

    cached: bool (but coming from controller this is a str "true"/"false")
        true returns data only form the db, false checks also on the node, defaults to true

    Returns
    -------
    [object]
        the list of all assets

    Raises
    ------
    EonError:
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        404: when there is no matching public_id in the local db
    """
    if cached and cached=="false":
        return
    else:
        return Asset.query.filter_by().all()

# def mint_asset(public_id, blind):
#     asset = get_an_asset(public_id, True)
#     print(asset)
#     if not asset:
#         raise EonError("Id not found",404)
#     asset_address = cu.get_new_address(asset.name)
#     print(asset_address)
#     ticker = asset.name[:2]
#     print(ticker)
#     domain = asset.external_url #regular expression - python extract domain from url
#     raw_issuance = cu.issue_lnft_asset(blind, asset_address, asset.name, ticker, domain)
#     print(raw_issuance)

def _save_changes(data):
    db.session.add(data)
    db.session.commit()
