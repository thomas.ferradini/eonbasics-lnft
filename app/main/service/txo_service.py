import uuid
import datetime
import math

from app.main.services import db
from app.main.util.crypto import cu
from app.main.model.txo import Txo
from app.main.util.eonerror import EonError
#from app.main.util.decorator import log_error
# TODO: set the logging preference in the config file

def save_new_txo(data):
    """
    Save a new txo in the local db

    If the same txid and voutn already exist, return a 409, duplicated content

    Parameters
    ----------
    data: dict
        an object with the following fields, as described in dto new_txo
        'txid', the id of the transaction
        'voutn', the index of the output of that transaction

    Returns
    -------
        [dict, int]
            an array which contains:
            0: the response_object with the following fields: 'status' success/fail,
            'message' and in case of success the 'public_id' of the new txo
            1: the return code for the successfull operation, 201

    Raises
    ------
    EonError:   
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        404: when there is something strange in the args (e.g. txid doesn't exist)
        409: when there is already this txo
    """
    try:
        existing_txo = Txo.query.filter_by(txid=data['txid'], voutn=data['voutn']).first()
        if not existing_txo:
            # check if txid and vout make sense:
            
            raw_tx = cu.get_raw_tx(data['txid'])
            if(not len(raw_tx['vout']) > data['voutn']):
                raise EonError('Txo does not exists.', 404)
           
            new_txo = Txo(
                public_id=str(uuid.uuid4()),
                txid=data['txid'],
                voutn=data['voutn'],
                value=data['value']
            )
            _save_changes(new_txo)
            response_object = {
                'status': 'success',
                'message': 'New txo registered.',
                'public_id': new_txo.public_id
            }
            return response_object, 201
        else:
            raise EonError('Txo already exists.', 409)
    except EonError as eerr:
        raise eerr
    except Exception as e:
        if('A JSON RPC Exception occured: -8' in str(e) or 'A JSON RPC Exception occured: -5' in str(e)): # -5 no such txid, -8 wrong parameter type/size
            raise EonError('Txo does not exists.', 404)
        else:
            print(e)
            raise EonError(e, 500)


def get_all_txos(args):
    """ 
    Get all the txos, cached return the latest status from local db, false queries the node 

    note that some txo in the db may not be coming from the connected node, 
    so we cannot assueme if a txo is not in the fresh retrieved utxos list
    then it must be spent.
    Therefore we just cycle through the utxos retrieved from the node and try to save them a new,
    the save operations takes care of checking if we already had it.
    
    Parameters
    ----------
    args is requet.args passed from the API controller

    args.get("cached"): str
        true/false, default to true and returns the latest status from the db
    args.get("page"): str
        page number, will be cast to int
    args.get("page_size"): str
        records per page, will be cast to int

    Returns:
    --------
    dict
        a dict with the pagianted resources:
        'total_record_count', the total number of resources on the lcoal db
        'page_size', how many records per page
        'page', pg number
        'records', [obj], array of ORM objects

    Raises
    ------
    EonError:   
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        400: when there is something strange in the args (e.g. cannot cast to int)
    """
    try:
        if(args.get("cached") and args.get("cached")=="false"):
            
            utxos = cu.get_utxos();
            for utxo in utxos:
                utxo['voutn'] = utxo['vout']
                exising_tx = Txo.query.filter_by(txid=utxo['txid'], voutn=utxo['voutn']).first()
                if not exising_tx:
                    new_txo = Txo(
                        public_id=str(uuid.uuid4()),
                        txid=utxo['txid'],
                        voutn=utxo['voutn'],
                        value=utxo['amount']
                    )
                    _save_changes(new_txo)
        local_txos = Txo.query.all()
        payload = {}
        payload['total_record_count']=len(local_txos)
        payload['page_size']= 20 if not args.get("page_size") else int(args.get("page_size"))
        payload['page']= 1 if not args.get("page") else int(args.get("page"))
        payload['records']=[]
        payload['page_size'] = payload['page_size'] if not payload['page_size'] > 200 else 200
        if(payload['page']<=0):
            payload['page']=1
        if(payload['page_size']*payload['page'] > payload['total_record_count']):
            #if indexes are out of bound, just return the last page
            payload['page'] = math.ceil(payload['total_record_count']/payload['page_size'])
        start_index = payload['page_size']*(payload['page']-1)
        end_index = (payload['page_size']*payload['page'])-1
        for txo in local_txos[start_index:end_index]:
            payload['records'].append(txo)
        return payload
    except ValueError as verr:
        # casting args to int fails
        raise EonError(verr, 400)
    except Exception as e:
        # get_utxos throws Exception when the node has connection problems for instance
        print(e)
        raise EonError(e, 500)

def get_a_txo(public_id, cached):
    """
    Returns details of a txo, when cached is false, query the node and get a fresh status

    Every time a non-cached request is done, its results are saved in the last_checked_ fields.

    Parameters
    ----------
    public_id: str
        the id on the db of the desired txo

    cached: bool (but coming from controller this is a str "true"/"false")
        true returns data only form the db, false checks also on the node, defaults to true

    Returns
    -------
    object
        the txo object

    Raises
    ------
    EonError:   
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        404: when there is no matching public_id in the local db
    """
    txo = None
    if(cached and cached=="false"):
        txo = refresh_txo_status(public_id)
    else:
        txo = Txo.query.filter_by(public_id=public_id).first()
        if not txo:
            raise EonError('Txo not found.', 404)
    return txo


def refresh_txo_status(public_id):
    """
    Returns the txo object from the ORM after updating its status with the node

    When checking, also save on the object the last_checked_status and the
    last_checked_time and blockhash.

    Parameters
    ----------
    public_id: str
        the id on the db of the desired txo

    Returns
    -------
    dict
        the txo object

    Raises
    ------
    EonError:   
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        404: when there is no matching public_id in the local db
    """
    txo = Txo.query.filter_by(public_id=public_id).first()
    if not txo:
        raise EonError('Txo not found.', 404)
    try:
        
        status = txo.status  # this will trigger the blockchain check
        txo.last_checked_status = status
        txo.last_checked_time = datetime.datetime.utcnow()
        bestblockhash = cu.get_best_blockhash()
        txo.last_checked_blockhash = bestblockhash
        _save_changes(txo)    
    except Exception as e:
        print(e)
        raise EonError(e, 500)
    return txo

def spend_a_txo(public_id, data):
    """
    Tries to spend the txo and returns it with the current status

    The spending can fail for many reasons, not your txo, txo reserved for a document.
    When it succeeeds, the new txid is recorded and the status updated

    TODO: now the method spends 0 to a change address, just posting the data
    we may want to configure the receiving address and the amount

    Parameters
    ----------
    public_id: str
        the id on the db of the desired txo to be spent
    data: dict
        the JSON body payload of the request, contains optional parameters:
        msg_hex: hex of the string to put in OP_RETURN
        amount: how much of the utxo to spend
        target_address: where to send to amount

    Returns
    -------
    obj
        the txo object with the refreshed state and the field "spent_in_tx" created

    Raises
    ------
    EonError:   
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        404: when there is no matching public_id in the local db
        400; bad request, e.g. message too long
    """
    msg_hex = None if not data.get('msg_hex') else data.get('msg_hex')
    if(len(bytes.fromhex(msg_hex))>80):
        raise EonError('message too long (max 80 bytes)', 400)

    target_address = None if not data.get('target_address') else data.get('target_address')
    amount = None if not data.get('amount') else data.get('amount')

    txo = Txo.query.filter_by(public_id=public_id).first()
    if not txo:
        raise EonError('Txo not found.', 404)
    try:
        
        txid = cu.spend_txo(txo.txid, txo.voutn, msg_hex)
        status = txo.status  # this will trigger the blockchain check
        txo.last_checked_status = status
        txo.last_checked_time = datetime.datetime.utcnow()
        bestblockhash = cu.get_best_blockhash()
        txo.last_checked_blockhash = bestblockhash
        txo.spent_with_txid = txid 
        _save_changes(txo)    
    except Exception as e:
        print(e)
        raise EonError(e, 500)
    return txo

def lock_a_txo(public_id):
    """
    Tries to lock a utxo

    The lock status is saved "in memory of the node", therefore we should
    also re-run this commaned whenever the node goes down.
    https://elementsproject.org/en/doc/0.18.1.7/rpc/wallet/lockunspent/

    Parameters
    ----------
    public_id: str
        the id on the db of the desired utxo to be locked

    Returns
    -------
    bool
        the success of the operation

    Raises
    ------
    EonError:   
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
        - a lock request needs an unlocked utxo, so if the txo is already locked, the method raises error
    """
    
    txo = Txo.query.filter_by(public_id=public_id).first()
    unlock_status = False
    if not txo:
        return True #nothing to do
    try:
        
        unlock_status = cu.lock_utxo(txo.txid, txo.voutn)
         
    except Exception as e:
        if("Invalid parameter, output already" in str(e) ): #we are trying to lock somethign already locked
            return True
        print(e)
        raise EonError(e, 500)
    return unlock_status

def unlock_a_txo(public_id):
    """
    Tries to lock a utxo

    The lock status is saved "in memory of the node", therefore we should
    also re-run this commaned whenever the node goes down.
    https://elementsproject.org/en/doc/0.18.1.7/rpc/wallet/lockunspent/

    Parameters
    ----------
    public_id: str
        the id on the db of the desired utxo to be locked

    Returns
    -------
    bool
        the success of the operation

    Raises
    ------
    EonError:   
        500: when cyrpto utils fails, the exception is catched and raised to the next level
        E.g. cannot connect the node
    """
    
    txo = Txo.query.filter_by(public_id=public_id).first()
    unlock_status = False
    if not txo:
        return True #nothing to do
    try:
        
        unlock_status = cu.unlock_utxo(txo.txid, txo.voutn)
         
    except Exception as e:
        if("Invalid parameter, expected locked" in str(e) ): #we are trying to unlock something already unlocked
            return True
        print(e)
        raise EonError(e, 500)
    return unlock_status

def _save_changes(data):
    db.session.add(data)
    db.session.commit()
