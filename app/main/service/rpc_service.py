import uuid
import datetime
import math
import os.path
import subprocess

from flask import current_app

from app.main.util.crypto import cu
from app.main.util.eonerror import EonError


def check_signature(data):
    """
    Returns the result of checking the sginature against the public key

    Parameters
    ----------
    data: dict
        'signed', str, the hex string of the signed bytes
        'message', str, the hex string of the original clear text message
        'public_key', str, the decode(utf-8) of the public key bytes in PEM format

    Returns
    -------
    dict
        'method', str, executed method
        'result', bool, true/false is the signature valid

    Raises
    ------
    EorError
        400, bad arguments, e.g. non hex strings
        500, server error

    """
    signed = data.get('signed') if data.get('signed') else ''
    message = data.get('message') if data.get('message') else ''
    serialized_public = data.get('serialized_public') if data.get('serialized_public') else ''
    try:
        valid_signature = cu.check_message(bytes.fromhex(signed), bytes.fromhex(message), serialized_public.encode('utf-8'))
        response = {'method':'check_signature','result':valid_signature}
        return response
    except ValueError as ve:
        raise EonError('Invalid data format, non hex message or non utf-8 serialized public key', 400)
    except Exception as e:
        print(e)
        raise EonError(str(e), 500)

def get_local_pub_key():
    """ 
    Returns the local public key, PEM encoding

    Get the poublic bytes and decode them with utf-8 before returning

    Parameters
    ----------

    Returns
    -------
    dict
        'method', str, executed method
        'result', str, utf-8 decode of the public key PEM bytes

    Raises
    ------

    """
    pub_key = cu.get_serialized_pub_key()
    utf8_pub_key = pub_key.decode("utf-8")
    response = {'method':'get_pub_key','result': utf8_pub_key}
    return response

def get_new_address():
    """
    Returns a new address from the node
    
    Parameters
    ----------
        data: dict
            'label', str, label for the address (optional)
            'type', str, 'legacy' or '' (optional)

    Returns
    -------
    dict
        'method', str, executed method
        'result', str, the new address

    Raises
    ------

    """
    new_address = cu.get_new_address(data.get('label'))
    response = {'method':'get_new_address','result': new_address}
    return response


def generator(msg_bytes, id):

    excluded_bytes = []
    if len(msg_bytes)>64:
        excluded_bytes=msg_bytes[64:]
        msg_bytes = msg_bytes[:64]

    #provable transform also does the padding right to the whole message if necessary
    provable_pack = cu.provable_sha256_transform(msg_bytes);
    #strip spaces:
    provable_pack['left']=provable_pack['left'].replace(' ','');
    provable_pack['right']=provable_pack['right'].replace(' ','');
    provable_pack['output']=provable_pack['output'].replace(' ','');

    #call eonbridge
    result = subprocess.run([current_app.config['EONBRIDGE_EXEC_PATH']+"/main generator"+" "+provable_pack['left']+" "+provable_pack['right']+" "+provable_pack['output']+" "+id+" "+current_app.config['EONBRIDGE_FILES_PATH']], 
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True, shell=True)

    #check result should be 1? return result.stdout
    #check the files
    expected_files = [id+"_vk", id+"_pvk", id+"_pk"]
    generator_output = {}
    for expected_file in expected_files:
        file_path = current_app.config['EONBRIDGE_FILES_PATH']+"/"+expected_file
        if(os.path.isfile(file_path)):
            generator_output[expected_file]=file_path

    if(len(excluded_bytes)>0):
        generator_output['excluded_bytes']=excluded_bytes

    generator_output['provable_hash'] = provable_pack['output']

    return generator_output

def prover(msg_bytes, id):

    excluded_bytes = []
    if len(msg_bytes)>64:
        excluded_bytes=msg_bytes[64:]
        msg_bytes = msg_bytes[:64]

    #provable transform also does the padding right to the whole message if necessary
    provable_pack = cu.provable_sha256_transform(msg_bytes);
    #strip spaces if any, eonbridge commands use spaces to distinguis parameters, so there's can't be any additional ones:
    provable_pack['left']=provable_pack['left'].replace(' ','');
    provable_pack['right']=provable_pack['right'].replace(' ','');
    provable_pack['output']=provable_pack['output'].replace(' ','');

    vk_path = current_app.config['EONBRIDGE_FILES_PATH']+"/"+id+"_vk";
    pk_path = current_app.config['EONBRIDGE_FILES_PATH']+"/"+id+"_pk";

    #call eonbridge
    result = subprocess.run([current_app.config['EONBRIDGE_EXEC_PATH']+"/main prover"+" "+pk_path+" "+vk_path+" "+provable_pack['left']+" "+provable_pack['right']+" "+provable_pack['output']+" "+id+" "+current_app.config['EONBRIDGE_FILES_PATH']], 
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True, shell=True)

    #check the files
    expected_files = [id+"_proof"]
    prover_output = {}
    for expected_file in expected_files:
        file_path = current_app.config['EONBRIDGE_FILES_PATH']+"/"+expected_file
        if(os.path.isfile(file_path)):
            prover_output[expected_file]=file_path

    if(len(excluded_bytes)>0):
        prover_output['excluded_bytes']=excluded_bytes

    return prover_output

def verifier(provable_hash_words, id):

    pvk_path = current_app.config['EONBRIDGE_FILES_PATH']+"/"+id+"_pvk";
    proof_path = current_app.config['EONBRIDGE_FILES_PATH']+"/"+id+"_proof";

    #call eonbridge
    command_line = current_app.config['EONBRIDGE_EXEC_PATH']+"/main verifier"+" "+pvk_path+" "+proof_path+" "+provable_hash_words+" "+id
    result = subprocess.run([command_line], 
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True, shell=True)
    #TODO:
    if(result.stdout.find("Verifier detects a corrupted proof")>=0):
        return -1

    return 1