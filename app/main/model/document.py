from app.main.model.txo import Txo
from app.main.services import db

"""
Document Model

Store the information of a document passed between two parties. Each party
declares their seal. The current owner controles the "current_seal", the
next owner (i.e. the receiving end) controls "next_seal".

Documents contain the clear-text hash of the document and the signed version.
The signatures are of both parties.

Documents in Eonpass are categorised in statuses according to the level of details 
they contain and the statuses of the two seals:

new: just created, it may be missing the seals and the signatures.
    If seals are present, they must be open (unspent)
preflight: document complete with current_seal (must be unspent),
           document hash, next_seal, sender_signed_hash
ready: document complete with current and next seals (must be unspent),
       document hash, sender hash, sender and receiver signed hash
sent: document complete of all fields, current_seal is a spent output

"""


class Document(db.Model):
    """ Document Model for storing related details """
    __tablename__ = "document"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    public_id = db.Column(db.String(100), unique=True, index=True)
    current_seal = db.Column(db.String(100), db.ForeignKey('txo.public_id'))
    next_seal = db.Column(db.String(100), db.ForeignKey('txo.public_id'))
    # hash of the documents
    document_hash = db.Column(db.String(100), nullable=False)
    # hash of the documents with the respective seals to be signed
    # by both parties
    document_with_seals_hash = db.Column(db.String(100))
    sender_signed_hash = db.Column(db.String(100))
    sender_public_key = db.Column(db.String(100))
    sender_and_receiver_signed_hash = db.Column(db.String(100))
    receiver_public_key = db.Column(db.String(100))

    @property
    def status(self):
        if not self.current_seal:
            return 'new'
        else:
            current_seal = Txo.query.filter_by(public_id=self.current_seal).first()
            if not current_seal:
                # public_id is not matching with anything
                return 'corrupt current seal id'
            current_seal_status = current_seal.last_checked_status

            next_seal = Txo.query.filter_by(public_id=self.next_seal).first()
            if not next_seal and self.next_seal:
                # public_id is not matching with anything
                return 'corrupt next seal id'
            next_seal_status = next_seal.last_checked_status if next_seal else 'missing'
            if(next_seal_status=='missing' and current_seal_status=='unspent'):
                return 'new'

            if current_seal_status == 'unspent' and next_seal_status != 'unspent':
                return 'corrupt next seal'

            if current_seal_status == 'unspent':
                # all these combinations should also check
                # if there are the respective public keys
                if self.document_with_seals_hash and \
                        self.sender_signed_hash and self.sender_public_key and \
                        self.current_seal and \
                        self.next_seal and \
                        (not self.sender_and_receiver_signed_hash or not self.receiver_public_key):
                    # both seals unspent
                    return 'preflight'
                elif self.document_with_seals_hash and \
                        self.sender_signed_hash and self.sender_public_key and \
                        self.current_seal and \
                        self.next_seal and \
                        self.sender_and_receiver_signed_hash and self.receiver_public_key:
                    return 'ready'
                else:
                    return 'new'
            elif self.document_with_seals_hash and \
                    self.sender_signed_hash and self.current_seal and \
                    self.next_seal and \
                    self.sender_and_receiver_signed_hash:  # current_seal spent
                return 'sent'
            else:
                # a spent seal should be present only in complete documents,
                # if you are missing fields, this document is corrupt
                return 'corrupt current seal'

    def __repr__(self):
        return "<Document current seal:'{}', next seal:'{}', document_hash:'{}', document_with_seals_hash:'{}', sender_signed_hash:'{}', sender_public_key:'{}', sender_and_receiver_signed_hash:'{}', receiver_public_key:'{}' >".format(
            self.current_seal, self.next_seal, self.document_hash,
            self.document_with_seals_hash, self.sender_signed_hash,
            self.sender_public_key, self.sender_and_receiver_signed_hash,
            self.receiver_public_key)
