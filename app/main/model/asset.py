import datetime

from bitcoinrpc.authproxy import JSONRPCException
from sqlalchemy import Index

from app.main.services import db
from app.main.util.crypto import cu

"""
Asset model

store the information about assets so that they can be used as seals. This table actas as
a buffer between Eonbasics and the blockchain node.

the status (spent or unspent) is not stored in the db but it's handled as a property which,
every time it's checked queries the node if the status is not already spent.

TODO:
rationalise the way node information are bound to Assets, for now we assume every instance
of Eonbasics will use Elements. But there may be more than one blockchain in the future.


"""


class Asset(db.Model):
    """ Asset Model for storing asset ids and related details """
    __tablename__ = "asset"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    public_id = db.Column(db.String(100), unique=True, index=True)
    description= db.Column(db.String(100), nullable=True) #metti True che almeno non da errori e poi cambi
    external_url= db.Column(db.String(100), nullable=False)
    image= db.Column(db.String(100), nullable=True)
    name= db.Column(db.String(100), nullable=False)
    attributes= db.Column(db.String(100), nullable=True)
    blockchain = db.Column(db.String(20)) #per ora non serve, ma utile nel momento in cui dovessimo usare un altra blockchain (bando)
    last_checked_time = db.Column(db.DateTime) # i last checked rimangono uguali
    last_checked_status = db.Column(db.String(10))
    last_checked_blockhash = db.Column(db.String(100))
    __table_args__ = (Index('unique_asset_data', "name", "external_url"), ) #evita righe duplicate (usando gli ash) e facilita la ricerca
    # different blockchain may have the same values.. for now we ignore this collision
    # the current scope focuses only on 1 underlying blockchain


    def __repr__(self):
        return "<ASSET name:'{}', external_url:'{}'>".format(
            self.name, self.external_url)

    @property
    def status(self):
        """
        Query the blockchain to see the status of this txo, if it's alraedy
        spent, just retun "spent"
        """
        if(self.last_checked_status == 'issued'):
            return self.last_checked_status
        else: #scrivere una funzione dentro crypto utils che controlla se una funzione è issued o no
            try:
                # status = cu.check_txout(self.txid, self.voutn) #andrebbe fatta un'altra funzione
                return "pending"
            except JSONRPCException as json_exception:
                print("A JSON RPC Exception occured: " + str(json_exception))
                return json_exception
            except Exception as general_exception:
                print("An Exception occured: " + str(general_exception))
                return general_exception

#qual è la funzione del nodo element che ti dice se sul nodo l'asset esiste? Potrebbe essere list issuances che ti dice se esiste o no quando gli passi l'id (?)
